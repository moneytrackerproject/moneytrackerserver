const ddbb = require('./mongoUtil');
const ObjectID = require('mongodb').ObjectID;

module.exports = {

  //---------------------------------------
  // Returns list of transactions... if no transactions return empty list
  listTransactions : function(req, res) {
    console.log('listtransaction');
    // recuperamos los datos de alta.
    try {
      // TODO Validación del token
      console.log('detail params user: ' + req.params.id_user);
      console.log('detail params contract: ' + req.params.id_contract);
      console.log('detail params skip: ' + req.body.skip);
      console.log('detail params limit: ' + req.body.limit);
      var skip = 0;
      var limit = 20;
      if (req.body.skip !== undefined) {
        skip = req.body.skip
      }

      if (req.body.limit !== undefined) {
        limit = req.body.limit
      }
      console.log(' skip: ' + skip);
      console.log(' limit: ' + limit);
      contract_data = {
        user_id : req.params.id_user,
        contract_id : req.params.id_contract
      };
      ddbb.getTransactions(contract_data,skip,limit, function(err, result) {
        if (err !== null) {
          res.status(500);
          res.send({err : err.message});
        } else {
          // Comprobamos si el contrato tiene movimientos
          if(result != null) {
            console.log(result);
            res.send(result);
          } else {
            res.send([]);
          }
        }
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },

  // Añadir una Operación
  addTransaction : function(req,res) {
    var transaction_data = req.body;

    // recuperamos el email para poder hacer el update del importe contrato
    contract_data = {
        _id : new ObjectID(req.params.id_user),
        'contracts.id' : req.params.id_contract
    };
    console.log('addTransaction');
    addTransactionAux(contract_data,transaction_data,function(err,result) {
      if (err !== null) {
        res.status(500);
        res.send({err : err.message});

      } else {
        if(result != null) {
          console.log(result);
          res.send(result);
        }

      }

    });
  },
  //  Hacemos traspaso cuenta Origen, cuenta destino e importe
  addTransfer : function(req, res) {
    console.log('addTransfer ');
    // recuperamos los datos de la operación.

    try {

      console.log('body: ' + req.body);
      var trasaction_data = req.body;
      //llamar a la funcion addTransaction

      contracts_data_ori = {
        _id : new ObjectID(req.params.id_user),
        'contracts.id' : req.params.id_contract
      };
      contracts_data_dest = {
        _id : new ObjectID(req.params.id_user),
        'contracts.id' : trasaction_data.destinationaccountid
      }
      console.log ('addTransfer - Traspaso origen');
      addTransactionAux(contracts_data_ori,trasaction_data,function(err,result) {
        if (err !== null) {
          res.status(500);
          res.send({err : err.message});

        } else if (result === null) {
          res.status(500);
          res.send({err : 'No se ha podido realizar la operación. Intente en unos minutos'});
        } else {
          console.log ('update origen realizado');
          // Cambiamos el signo a la operación
          var aux_amount = trasaction_data.amount * -1;
          trasaction_data.amount = aux_amount;
          //llamar a la funcion addTransaction

          console.log('addTransfer - Traspaso destino');
          addTransactionAux(contracts_data_dest,trasaction_data,function(err,result) {
            if (err !== null) {
              res.status(500);
              res.send({err : err.message});

            }  else if (result === null) {
              res.status(500);
              res.send({err : 'No se ha podido realizar la operación sobre cuenta destino.'});
            } else {
              console.log(result);
              res.send(result);
            } 
          });
        } 
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  }
}

// Añadir una Operación
addTransactionAux = function(filter_data,transaction_data, callback) {
  console.log('addTransactionAux ');
  // recuperamos los datos de la operación.
  try {

    var new_amount = 0;

    //Devolvemos únicamente el contrato seleccionado
    ddbb.getUserWithFields(filter_data, {'contracts.$':1}, function(error,data) {
      if (error !== null) {
        console.log("error get user 1" + error.message);
        callback(error,null);
      } if (data == null) {
        console.log("origin contract not fount" + error.message);
        var err = {
          code:'userNotFound',
          message:'usuario/contrato no localizado'
        }
        callback(err,null);
      } else {
        //Actualizamos el saldo del contrato.
        console.log("numero de operaciones " + data.contracts.length);

        console.log(data.contracts[0].primaryAmount.amount + " + " + parseInt(transaction_data.amount));

        new_amount = parseInt(data.contracts[0].primaryAmount.amount) + parseInt(transaction_data.amount);

        update_data = {'contracts.$.primaryAmount.amount' : new_amount};

        ddbb.update(filter_data, update_data,function(err, result) {
          if (err !== null){
            callback(err,null);
          } else if (result.nModified == 0) {
            callback({message:"No se ha localizado el contrato origen de la operación"},null);
          }

          //insertamos en movimiento en la BD. transaction
          var transaction_in = createTransfer(filter_data._id.toHexString(), 
                                              filter_data['contracts.id'],
                                              transaction_data.description,
                                              transaction_data.concept,
                                              transaction_data.amount,
                                              transaction_data.currency,
                                              new_amount);

          ddbb.newTransaction(transaction_in, function(error,data) {
            if (error !== null) {
                callback(err,null);
              } else {
                callback(null,{msg: 'Operation succesfull'});
            }
          });
        });
      }
    });
  } catch (err) {
    console.log(err);
    res.status(500);
    res.send({err : err.message});
  }
}

// Funcion que genera una transacción con el formato de los campos esperado
createTransfer = function(user_id_in, 
                          contract_id_in,
                          description_in,
                          label_in,
                          ammount_in,
                          currency_in,
                          balance_in) {

  var newData = {
    user_id: user_id_in,
    contract_id: contract_id_in,
    date: new Date(),
    description: description_in,
    label: label_in,
    category: 9999,
    'category-description' : "Pendiente categorizar",
    'parsed-amount': {
        value: ammount_in,
        currency: currency_in
    },
    'parsed-accounting-balance': {
        value: balance_in,
        currency: currency_in
    },
    test: {
        status: "success",
        label: "Operación Manual"
    }
  }
  return newData;

}