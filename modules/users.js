// import libraries & modules
const config = require('../config');
const jwt = require('jsonwebtoken');
const ddbb = require('./mongoUtil');
const ObjectID = require('mongodb').ObjectID;
const nodemailer = require('nodemailer');
//const imc = require('./modules/imc');
const imc = require('./imc');

//Creamos la cache que utilizaremos para el control de 1 solo login activo y para el contros de autorizaciones
const NodeCache = require( "node-cache" );
const myCache = new NodeCache( { stdTTL: 10*60, checkperiod: 60 } );

//Implementation & export public functions
module.exports = {
  //---------------------------------------
  // Create a new user en c_user collection
  signUp : function(req, res) {
    console.log('signUp ');
    // recuperamos los datos de alta.
    try {
      var user_data = req.body;
      user_data.status = 'inactive';
      // TODO Validacion de los datos de entrada
      ddbb.newUser(user_data, function(error,data) {
        // TODO Envo de correo a la dirección del cliente
        if (error !== null) {
          if (error.code == 11000) {
            res.status(409);
            res.send({err:'Email ya registrado'});
          } else {
            res.status(500);
            res.send({err: error.message});
          }
        } else {
          //Enviamos un correo para validar el usuario

          res.send({msg:'Solo estás a un paso de poder disfrutar de Money tracker. Se ha enviado un enlace para validar su cuenta','_id':data._id});
          user_data.action = 'validateUser';
          sendSecurityMail(user_data.email,
                          '[Money Tracker] Validación de correo electronico',
                          'Validar su correo',
                          config.frontBaseURI + 'validateuser?token=' + encodeToken(user_data),
                          function(err,result){
            if (err !== null){
              res.status(500);
              res.send({err : err.message,code: 'errorSendingEmail'});
            } else {
              console.log('sendSecurityMail' + result);
              res.send({msg: 'Se ha enviado un mensaje para activar la cuenta al correo indicado'});
            }
          })
        }

      });
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },

  //---------------------------------------
  // returns all user's information
  getDetail : function(req, res) {
    console.log('getDetail ');
    // recuperamos los datos de alta.
    try {
      // TODO Validación del token
      //checkTokenClient(token,req.params.id_user)
      console.log('detail params: ' + req.params.id_user);
      user = {
        _id : new ObjectID(req.params.id_user)
      }
      ddbb.getUser(user, function(err, data) {
        if (err !== null) {
          res.status(500);
          res.send({err : err.message});
        } if (data == null) {
          res.status(404);
          res.send({err : 'Usuario no localizado',
                    code: 'dataNotfound'});
        } else {
          res.send(data);
        }
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },
  //---------------------------------------
  // Validate user & password, create a new Token and stores new Token in cache
  login : function(req, res) {
    console.log('login');
    console.log(req.headers);
    try {
      // Recuperams la información
      user = {
        email : req.body.email,
        password : req.body.password
      };
      ddbb.getUser(user, function(err,data){
        if (data == null ) {
          res.status(401); // Unauthorized
          res.send({err:'Usuario / password no valido',
                    code:'invalidCredentials'});
        }
        // Si el usuario ya está logado y no se ha marcado "forzar"
        else if (myCache.get(data._id.toHexString()) != undefined &&
                   req.headers['forcelogin'] !== 'true') {
          console.log('force?' + req.headers['forcelogin']);
          res.status(409); // conflict
          res.send({err:'Usuario ya logado. Finalice sesión previa',
                    code:'userAlreadyLogged'});
        } else if (data.status == 'inactive') {
          res.status(409); // conflict
          res.send({err:'Usuario no activo. Revise el correo',
                    code:'userInactive'});
        } else {
          // Añadimos el elemento a la cache
//          var authToken = imc.createToken(data._id.toHexString());
          var authToken = imc.createToken(data);
          myCache.set(data._id.toHexString(),authToken, 10*60, function(){});
          res.send({msg:'login correcto', _id:data._id, 'auth_token': authToken});
        }
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message,
                code:'unexpectedError'});
    }
  },
  //---------------------------------------
  // If de user is logged retrieve data from cache
  logout : function(req, res) {
    console.log('logout');
    try {
      // Recuperams la información
      id_user = req.params.id_user;
      user = {
        _id : new ObjectID(id_user)
      }
      console.log('logout: ' + id_user);
      user_token = myCache.get(id_user);
      console.log('user_token: ' + user_token);

      if ( user_token == undefined) {
        // El usuario no está logado
        res.status(409); // conflict
        res.send({err:'Usuario no está logado.'});
      } else {
        // Eliminamos el elemento a la cache.
        myCache.del(id_user);
        // Eliminamos el usuario de la bbdd.
        res.send({msg:'logout correcto', _id:id_user});
      }
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },
  //---------------------------------------
  // update personal data
  updateUser : function(req, res) {
    console.log('updateUser');
    // recuperamos los datos de alta.
    try {
      // TODO Validación del token
      console.log('detail params: ' + req.params.id_user);
      user = {
        _id : new ObjectID(req.params.id_user)
      }
      userData = req.body;

      ddbb.update(user, userData, function(err, result) {
        if (err !== null) {
          res.status(500);
          res.send({err : err.message});
        } else {
          // TODO Comprobamos si el usuario contiene contractos
          console.log(result);
          res.send({msg:'La información se ha actualizado correctamente'});
        }
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },
  //---------------------------------------
  // delete user informatión (waterfall) from ddbb
  delUser : function(req, res) {
    console.log('delUser');
    try {
      // Recuperams la información
      id_user = req.params.id_user;
      user = {
        _id : new ObjectID(id_user)
      }
      console.log('delUser: ' + id_user);
      user_token = myCache.get(id_user);
      if (myCache.get(id_user) !== undefined) {
        ddbb.delUser(user, function(err,result){
          if (err != null ) {
            res.status(500); // Error en el borrado
            res.send({err:'No se ha podido eliminar el usuario: ' + err.message});
          } else {
            // Añadimos el elemento a la cache
            console.log('deleted ok: ' + result);
            myCache.del(id_user);
            res.send({msg:'Borrado correcto', _id:id_user});
          }
        })
      } else {
        res.status(409); // NO autorizado
        res.send({err:'Login requiered'});
      }
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },
  //---------------------------------------
  // Returns list of contracts... if no contracts return empty list
  listContracts : function(req, res) {
    console.log('listContracts');
    // recuperamos los datos de alta.
    try {
      // TODO Validación del token
      console.log('detail params: ' + req.params.id_user);
      user = {
        _id : new ObjectID(req.params.id_user)
      }
      ddbb.getUser(user, function(err, result) {
        if (err !== null) {
          res.status(500);
          res.send({err : err.message});
        } else {
          // Comprobamos si el usuario contiene contractos
          if(result != null && result.hasOwnProperty('contracts') && result.contracts.length > 0) {
            res.send(result.contracts);
          } else {
            res.send([]);
          }
        }
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },
  // Returns information about a contract
  getContract : function(req, res) {
    console.log('getContract');
    // recuperamos los datos de alta.
    try {
      // TODO Validación del token
      console.log('detail params: ' + req.params.id_user + '/' + req.params.id_contract);
      var id_contract = req.params.id_contract;
      var user = {
        _id : new ObjectID(req.params.id_user)
      }
      ddbb.getUser(user, function(err, result) {
        if (err !== null) {
          res.status(500);
          res.send({err : err.message});
        } else {
          // Comprobamos si el usuario contiene contractos
          if(result != null && result.hasOwnProperty('contracts') && result.contracts.length >= id_contract) {
            res.send(result.contracts[id_contract]);
          } else {
            res.status(400)
            res.send({msg:'contrato no válido', code:'invalidContractID'});
          }
        }
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },
  // Returns information about a contract
  updateContract : function(req, res) {
    console.log('listContracts');
    // recuperamos los datos de alta.
    try {
      // TODO Validación del token
      console.log('detail params: ' + req.params.id_user + '/' + req.params.id_contract);
      var id_contract = req.params.id_contract;
      var user = {
        _id : new ObjectID(req.params.id_user),
        'contracts.number': id_contract
      }
      var update = {'contract.$' : req.body};
      ddbb.update(user, update, function(err, result) {
        if (err !== null) {
          res.status(500);
          res.send({err : err.message});
        } else {
          // Comprobamos si el usuario contiene contractos
          res.send({msg:'Actualización correcta', data: result});
        }
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },
  //---------------------------------------
  // Add new Contract to user
  addContract : function(req, res) {
    console.log('addContract');
    // recuperamos los datos de alta.
    try {
      // TODO Validación del token
      console.log('detail params: ' + req.params.id_user);
      user = {
        _id : new ObjectID(req.params.id_user)
      }
      contract = req.body;
      ddbb.addContract(user, contract, function(err, result) {
        if (err !== null) {
          res.status(500);
          res.send({err : err.message});
        } else {
          // TODO Comprobamos si el usuario contiene contractos
          console.log(result);
          res.send({msg:'working on it...correct'});
        }
      })
    } catch (err) {
      console.log(err);
      res.status(500);
      res.send({err : err.message});
    }
  },
  //---------------------------------------
  // Funcion que manda un correo a la dirección indicada con el enlace a la página para resetear contraseña
  sendResetPasswordMail : function(req, res) {
    console.log('Reset receive ' + req.body.email);
    // comprobamos que el usuario existe
    var userFound = false;
    user = {
      email : req.body.email
    };
    ddbb.getUser(user, function(err,data){
      if (err !== null){
        res.status(500);
        res.send({err : 'No se ha podido enviar el correo de reseteo. Revise la dirección de correo', code: 'emailNotSent'});
      } else {
        // Generamos
        user.action = 'changePassword';
        var text = 'Buenos días,\n Pulse el siguiente enlace para modificar su contraseña ' +
                     +
                    '\n\n Un saludo desde el equipo de MoneyTracker';

        // Enviamos la contraseña
        console.log('text: ' + text);
        sendSecurityMail(user.email,
                         '[money tracker] Reseteo de contraseña',
                         'resetear la contraseña',
                         config.frontBaseURI + 'changepassword?token=' + encodeToken(user),
                         function(err,result){
          if (err !== null){
            res.status(500);
            res.send({err : err.message,code: 'errorSendingEmail'});
          } else {
            console.log('sendSecurityMail' + result);
            res.send({msg: 'Se ha mandado el correo a la dirección indicada'});
          }
        });
      }
    });
  },
  //---------------------------------------
  // Servicio que manda el email de validación.
  actions : function(req, res) {
    console.log('actions');
    //var action_in = req.body.action;

    console.log(req.headers);
    console.log(req.params);
    //console.log(req.body.password);

    var data = decodeToken(req.headers.token);
    if (data == null) {
      console.log('Error, token no valido');
      res.status(401)
      res.send({msg:'Error, token no valido',code:'invalidToken'})
    } else if (req.params.action_id !== data.action) {
      console.log('Error, Accion o usuario no corresponde con el token');
      res.status(401)
      res.send({msg:'Error, Accion no corresponde con el token',code:'invalidToken'})
    } else {
      switch(data.action) {
        case 'changePassword':
          filter_data = {email: data.email};
          update_data = {password: req.body.password};
          break;
        case 'validateUser':
          filter_data = {email: data.email};
          update_data = {status: 'active'};
          break;
      }
      console.log('filter_data ' + filter_data);
      console.log('update_data ' + update_data);
      ddbb.update(filter_data, update_data,function(err, result) {
        if (err !== null){
          res.status(500);
          res.send({err : err.message,code: 'errorUpdateUser'});
        } else {
          console.log('sendSecurityMail' + result);
          if (JSON.parse(result).n == 0) {
            res.status(400);
            res.send({err : 'No se ha encontrado el email asociado',code: 'emailNotFound'});
          } else {
            res.send({msg: 'Petición procesada correctamente'});
          }
        }
      });
    }
  }

};

sendSecurityMail = function(email_in,subject_in, text_in, link_in, callback) {
  // Generamos un token de 15 minutos para incluir en la URL del correo
  var transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: config.appEmail,
      pass: config.appEmailPwd
    }
  });
  var mailOptions = {
    from: config.appEmail,
    to: email_in,
    subject: subject_in,
    text: "Este correo se ha generado automáticamente. por favor no responda a este correo",
    html: 'pulsa <a href=' + link_in + '>aquí</a> para ' + text_in
  };
  transporter.sendMail(mailOptions, callback);
}

// Rutina de cifrado para el tratamiento de mails
encodeToken = function(data) {
  return jwt.sign(data, config.secret, { expiresIn: '1h' });
}

// Rutina de descifrado para el tratamiento de mails
decodeToken = function(data) {
  try {
    var decoded = jwt.verify(data, config.secret);
    return decoded;
  } catch (e) {
    console.log('Error al validar el token' + e);
    return null
  }
}

/*
  // Implementation of private functions
  checkTokenClient : function(token,id_user){
    return (token == id_user);
  },

  checkTokenContract : function(token,user_id,contract_id){
    return (token == id_user);
  },

  createToken : function(user_data) {
    return user_data;
  }

*/


/* Estructura de datos propuesta
user : {
  _id : uid,
  first_name : String,
  last_name : String,
  email : String,
  mobilePhone : Number,
  password : String,
  birthDate: Date,
  postal_code : Number,
  direction : String,
  status : String, (active, unconfirmed, blocked, inactive),
  alerts : {
    key:value
  },
  aud_date : Date,

  contracts : {
    _id : uid,
    type: String,
    number: String,
    alias: String,
    currency : String
    balance : Number
    tracked: Boolean,
    aud_date : Date
  }
}
*/
