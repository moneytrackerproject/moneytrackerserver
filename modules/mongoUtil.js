// Declaración de las variables de entorno
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const config = require('../config.js');

// Alta de usuario
// Recibe un documento a insertar y una funcion para manejar
module.exports.newUser = function(user_data, callback) {
  console.log("newUser");
  // Connect to mongo database
  MongoClient.connect(config.mlab.database_uri, function(err, client) {
    // console.log("Connected at " + config.mlab.database_uri);
    if (err !== null) {
      console.log(err);
      callback(err,null);
    } else {
      var db = client.db('techu');
      var collection = db.collection('c_users');
      collection.insertOne(user_data, function(err, document){
      //console.log('user id: ' + document._id);
        if (err != null ) {
          console.log(err);
          callback(err,null);
        } else if (user_data == null || user_data.length == 0) {
          callback('No se ha encontrado el usuario', null);
        } else {
          callback(null,user_data);
        }
      })
    }
    client.close();
  })
}
// Consulta de usuario
// Recibe un id de documento y devuelve la información almacenada (si existe)
module.exports.getUserWithFields = function(user_data,fields, callback) {
  console.log('con fields');
  getUserAux(user_data,fields, callback);
}

// Consulta de usuario
// Recibe un id de documento y devuelve la información almacenada (si existe)
module.exports.getUser = function(user_data, callback) {
  console.log('sin fields');
  getUserAux(user_data, {}, callback);
}

// Borrado de usuario
// Recibe un id de usuario para elimiar el usuario de la base de datos
module.exports.delUser = function(user_data, callback) {
  console.log("delUser");
  // Connect to mongo database
  MongoClient.connect(config.mlab.database_uri, function(err, client) {
    // console.log("Connected at " + config.mlab.database_uri);
    if (err !== null) {
      console.log(err);
      callback(err,null);
    } else {
      var db = client.db('techu');
      var collection = db.collection('c_users');

      collection.deleteOne(user_data, function(err, document) {
        if (err !== null) {
          console.log(err);
          callback(err,null);
        } else {
          console.log('deleted' + document);
          callback(null, document);
        }
      });
    }
    client.close();
  })
}

// Añadir contrato a usuario
// Recibe un id de usuario para elimiar el usuario de la base de datos
module.exports.addContract = function(user, contract, callback) {
  console.log("addContract");

  // Connect to mongo database
  MongoClient.connect(config.mlab.database_uri, function(err, client) {
        // console.log("Connected at " + config.mlab.database_uri);
    if (err !== null) {
      console.log(err);
      callback(err,null);
    } else {
      var db = client.db('techu');
      var collection = db.collection('c_users');
      // TODO estaría bien validad si el contrato ya existe...asignación de un id.
      collection.update(user, {'$push':{'contracts':contract}}, function(err, result) {
        if (err !== null) {
          console.log(err);
          callback(err,null);
        } else {
          console.log('addContract' + result);
          callback(null, result);
        }
      });
    }
    client.close();
  })
}

// Añadir un movimiento a un Contrato.
// Recibe un documento a insertar y una funcion para manejar
module.exports.newTransaction = function(transaction_data, callback) {
  console.log("newTransaction");
  // Connect to mongo database
  MongoClient.connect(config.mlab.database_uri, function(err, client) {
    // console.log("Connected at " + config.mlab.database_uri);
    if (err !== null) {
      console.log(err);
      callback(err,null);
    } else {

      var db = client.db('techu');
      var collection = db.collection('c_transactions');

      collection.insertOne(transaction_data, function(err, document){
  //      console.log('contracts id: ' + document._id);
        if (err != null ) {
          console.log(err);
          callback(err,null);
        } else {
          callback(null, document);
        }
      });
    }
    client.close();
  })
}

// Listar movimientos de un contrato
// Recibe un contrato para consultar sus movimientos
module.exports.getTransactions = function(contract,skip, limit, callback) {
  console.log("gettransaction");
  // Connect to mongo database
  MongoClient.connect(config.mlab.database_uri, function(err, client) {
    // console.log("Connected at " + config.mlab.database_uri);
    if (err !== null) {
      console.log(err);
      callback(err,null);
    } else {
      var db = client.db('techu');
      var collection = db.collection('c_transactions');

      console.log("contract filter: " + JSON.stringify(contract));

      collection.find(contract, function(err, cursor){
  //      console.log('contracts id: ' + document._id);
        if (err != null ) {
          console.log('ERROR getTransactions' + err);
          callback(err,null);
        } else {
          //cursor.skip(skip).limit(limit).toArray(function (err,documents) {
            cursor.toArray(function (err,documents) {
            console.log('------listado de movimientos:     ' + JSON.stringify(documents));
            callback(null, documents);
          })
        }
      });
    }
    client.close();
  })
}

// Actualizamos datos de los usuarios (acción genérica)
// Recibe los criterios de los usuarios y los campos a actualizar
module.exports.update = function(filter, data, callback) {
  console.log("updateUserData");

  // Connect to mongo database
  MongoClient.connect(config.mlab.database_uri, function(err, client) {
    // console.log("Connected at " + config.mlab.database_uri);
    if (err !== null) {
      console.log(err);
      callback(err,null);
    } else {
      var db = client.db('techu');
      var collection = db.collection('c_users');
      // TODO estaría bien validad si el contrato ya existe...asignación de un id.
      collection.update(filter, {'$set':data}, function(err, result) {
        if (err !== null) {
          console.log(err);
          callback(err,null);
        } else {
          console.log('updateUserData' + result);
          callback(null, result);
        }
      });
    }
    client.close();
  })
}

getUserAux = function(user_data,fields, callback) {
  console.log("getUser");
  // Connect to mongo database
  MongoClient.connect(config.mlab.database_uri, function(err, client) {
    //TO dontrolar el error de la conexión
    if (err !== null) {
      console.log("Error al conectar " + err.message);
      console.log(err);
      callback(err,null);
    } else {
      // console.log("Connected at " + config.mlab.database_uri);
      var db = client.db('techu');
      var collection = db.collection('c_users');
      console.log ('user_data --> ' + JSON.stringify(user_data));
      console.log ('fields -->' + JSON.stringify(fields));
      collection.findOne(user_data,{fields}, function(err, document) {
        if (err !== null) {
          console.log(err);
          callback(err,null);
        } else {
          console.log('document' + JSON.stringify(document));
          callback(null, document);

        }
      });
    }
    client.close();
  })
}
