// import libraries & modules
const config = require('../config');
const jwt = require('jsonwebtoken');
const NodeCache = require( "node-cache" );

//Implementation & export public functions
module.exports = {
  //---------------------------------------

// metodo para validar el IMC
  validateIMC : function(req, res, next) {
    if (config.imcdisable) {
      next();
    } else {

        var token = req.headers['auth_token'];

        console.log("obtenemos el token +", token);
      //  console.log("prueba");
      // decode token
        var imcOK = false;
        if (token) {
           console.log('parametros->', req.params);
           console.log('id_user-->', req.params.id_user);
           if (req.params.id_user == undefined) {
              imcOK = false;
           } else if (req.params.id_contract == undefined){
              imcOK = checkTokenClient(token,req.params.id_user);
           } else {
             imcOK = checkTokenContract(token,req.params.id_user, [req.params.id_contract]);
           }

           if (imcOK) {
             console.log('debemos hacer next');
             next();
           } else {
             res.status(403);
             res.send({err : 'Error de autenticación',
                       code:'Failed to authenticate token'});
           }

        } else {

          // if there is no token
          // return an error

          res.status(403);
          res.send({err : 'No se ha informado token',
                    code:'no token provided'});
        }
      }
  },


  createToken : function(datos_token) {
    var token = jwt.sign(datos_token, config.secret,{ expiresIn: 60*60 });
    return token;
  }

};


  // Implementation Control del canal cliente
  checkTokenClient = function(token, user_id){
    try {
      var decoded = jwt.verify(token, config.secret);
      console.log(JSON.stringify(decoded) + ' vs '+ user_id);
      return decoded._id == user_id;
    } catch (e) {
      console.log('error al validar el token ->', e.message);
      return false
    }

  };

  // Implementation Control del canal contrato

  checkTokenContract = function(token,user_id,contracts){
    //return (token == client);

    try {
      var decoded = jwt.verify(token, config.secret);
      console.log(JSON.stringify(decoded) + ' vs '+ user_id);
      if ( decoded._id == user_id) {
        for (c of contracts) {
          // comprobamos si al filtrar por ID, tiene longitud igual a 0 (si no existe)
          if (decoded.contracts.filter(contract => contract.id == c).length == 0){
              return false;
          }
        }
        return true;
      } else {
        return false;
      }
    } catch (e) {
      console.log('error al validar el token ->', e.message);
      return false
    }

  };

/* Estructura de datos propuesta
user : {
  _id : uid,
  first_name : String,
  last_name : String,
  email : String,
  mobilePhone : Number,
  password : String,
  birthDate: Date,
  postal_code : Number,
  direction : String,
  status : String, (active, unconfirmed, blocked, inactive),
  alerts : {
    key:value
  },
  aud_date : Date,

  contracts : {
    _id : uid,
    type: String,
    number: String,
    alias: String,
    currency : String
    balance : Number
    tracked: Boolean,
    aud_date : Date
  }
}
*/
