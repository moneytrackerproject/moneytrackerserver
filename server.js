// Server aplication libraries
const express     = require('express');
const app         = express();
const bodyParser  = require('body-parser');

// requiere modules
const config = require('./config');
const utils = require('./modules/utils');
const users = require('./modules/users');
const transactions = require('./modules/transactions');
const imc = require('./modules/imc');

console.log('review secret ' + config.secret);
console.log('review uri ' + config.mlab.database_uri);

const base_url = '/moneyTracker/v1';


// Configurating server
const PORT = 3000;



//app.set('superSecret', config.secret); // secret variable
app.use(bodyParser.json());

app.listen(PORT);
console.log("Application listening at localhost:"+PORT+"/");


// Services routes
app.get(base_url+'/', utils.apiMessage);
// Api endpoint
app.get(base_url+'/api', utils.underConstruction);
// Login de usuario
app.post(base_url+'/login', users.login);
// Alta de usuario
app.post(base_url+'/new', users.signUp);
// Detalle de usuario
//app.get(base_url+'/user/:id_user', users.getDetail);
app.get(base_url+'/user/:id_user',imc.validateIMC, users.getDetail);
// Borrado de usuario
app.delete(base_url+'/user/:id_user',imc.validateIMC, users.delUser);
// Logout de usuario
app.post(base_url+'/user/:id_user/logout',imc.validateIMC, users.logout);
// actualizar los datos del usuario
app.put(base_url+'/user/:id_user',imc.validateIMC, users.updateUser);
// alta de una nueva cuenta
app.post(base_url+'/user/:id_user/contracts',imc.validateIMC, users.addContract);
// Listado de contratos
app.get(base_url+'/user/:id_user/contracts',imc.validateIMC, users.listContracts);
// Alta de una operacion
//app.post(base_url+'/user/:id_user/contracts/:id_contract/transactions', utils.addOperation);
app.post(base_url+'/user/:id_user/contracts/:id_contract/transactions', imc.validateIMC, transactions.addTransaction);
// Listado de operaciones con filtros por contrato y atributos de la transaccion
app.get(base_url+'/user/:id_user/contracts/:id_contract/transactions', imc.validateIMC, transactions.listTransactions);
// Servicio para solicitar el desbloqueo: envio de email para el desbloqueo
app.post(base_url+'/forgotpassword', users.sendResetPasswordMail);
//puerto de escucha de acciones administrativas
app.post(base_url+'/user/action/:action_id', users.actions);
//traspaso entre 2 cuentas);
app.post(base_url+'/user/:id_user/contracts/:id_contract/transfer', imc.validateIMC, transactions.addTransfer);


//TODO Otra forma m�s optima de reorganizaci�n
