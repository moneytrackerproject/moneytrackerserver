'use strict';
var mocha = require('mocha');
var chai = require('chai');
var chaihttp = require('chai-http');

chai.use(chaihttp);

var should = chai.should();
//var expect = chai.expect;

var server = require('../server'); //lanzar el server

describe('Signup test',
  function() {

    //Registro OK

    var id_cliente = "";
    var id_token ="";

    it('Signup - Registro Ok',
      function(done) {
          chai.request('http://localhost:3000')
          .post('/moneyTracker/v1/new')
          .set('content-type', 'application/json')
          .send({"email": "mjcasas.test@gmail.com",
                 "password": "1234567",
                 "name": "Test",
                 "surname" : "Prueba"
           })
          .end(
              function(err,res) {
                console.log("Request has ended");
                console.log('err' + err);
                console.log('body<' + res.body);

                //console.log(res);
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.have.property('_id');
                //res.body.should.have.property('auth_token');
                res.body.msg.should.be.eql("Solo estás a un paso de poder disfrutar de Money tracker. Se ha enviado un enlace para validar su cuenta");
                //id_token = res.body.auth_token;
                //console.log (id_cliente + '/'+ id_token);
                console.log (res.body._id + '/'+ res.body.auth_token);
                id_cliente = res.body._id;
                //id_token = res.body.auth_token;
                console.log ('id_cliente --> ' + id_cliente );
                done();
              }
          )
      }
    ),

// Resgistro duplicado

it('Signup - Registro Duplicado',
  function(done) {
      chai.request('http://localhost:3000')
      .post('/moneyTracker/v1/new')
      .set('content-type', 'application/json')
      .send({"email": "mjcasas.test@gmail.com",
             "password": "1234567",
             "name": "Test",
             "surname" : "Prueba"
       })
      .end(
          function(err,res) {
            console.log("Request has ended");
            console.log('err-->' + err);

            //console.log(res);
            res.should.have.status(409);
            res.should.be.json;
            res.body.err.should.be.eql("Email ya registrado");
            done();
          }
      )
  }
),

// test asociados al LOGIN / LOGOUT

// Login Usuario
// login KO - Usuario / password incorrecto

    it('login - Usuario / password incorrecto',
      function(done) {
          chai.request('http://localhost:3000')
          .post('/moneyTracker/v1/login')
          .set('content-type', 'application/json')
          .send(
                 {"email": "mjcasas.test@gmail.com",
                  "password": "123456",
	                }
          )
          .end(
              function(err,res) {
                console.log("Request has ended");
                console.log(err);
//                console.log(res);
                res.should.have.status(401);
                res.should.be.json;
                res.body.err.should.be.eql("Usuario / password no valido");
                res.body.code.should.be.eql("invalidCredentials");
                console.log ('retorno -> ' + res.body.code);
                done();
              }
          )
      }
    ),

    // login KO - Usuario inactivo

        it('login - Usuario inactivo',
          function(done) {
              chai.request('http://localhost:3000')
              .post('/moneyTracker/v1/login')
              .set('content-type', 'application/json')
              .send(
                     {"email": "mjcasas.test@gmail.com",
                      "password": "1234567",
    	                }
              )
              .end(
                  function(err,res) {
                    console.log("Request has ended");
                    console.log(err);
    //                console.log(res);
                    res.should.have.status(409);
                    res.should.be.json;
                    res.body.err.should.be.eql("Usuario no activo. Revise el correo");
                    res.body.code.should.be.eql("userInactive");
                    console.log ('retorno -> ' + res.body.code);
                    done();
                  }
              )
          }
        ),

// login OK
            it('login - OK',
              function(done) {
                  chai.request('http://localhost:3000')
                  .post('/moneyTracker/v1/login')
                  .set('content-type', 'application/json')
                  .send(
                         {"email": "mjulia.casaslara@gmail.com",
                          "password": "1234567",
        	                }
                  )
                  .end(
                      function(err,res) {
                        console.log("Request has ended");
                        console.log(err);
//                        console.log(res);
                        res.should.have.status(200);
                        res.should.be.json;
                        res.body.should.have.property('_id');
                        res.body.should.have.property('auth_token');
                        console.log (res.body._id + '/'+ res.body.auth_token);
                        id_cliente = res.body._id;
                        id_token = res.body.auth_token;
                        console.log ('id_cliente --> ' + id_cliente );
                        console.log ('auth token /'+ id_token);
                        res.body.msg.should.be.eql("login correcto");
                        done();
                      }
                  )
              }
            )


// login KO - usuario ya logado
    it('login - Usuario ya Logado',
        function(done) {
            chai.request('http://localhost:3000')
            .post('/moneyTracker/v1/login')
            .set('content-type', 'application/json')
            .send(
                 {"email": "mjulia.casaslara@gmail.com",
                  "password": "1234567",
	                }
                )
            .end(
                function(err,res) {
                  console.log("Request has ended");
                  console.log(err);
                  //console.log(res);
                  res.should.have.status(409);
                  res.should.be.json;
                  res.body.err.should.be.eql("Usuario ya logado. Finalice sesión previa");
                  res.body.code.should.be.eql("userAlreadyLogged");
                  console.log ('retorno -> ' + res.body.code);
                  done();
                }
              )
          }
      )


// Detalle de un Usuario
    it('getDetail - Datos Ok',
      function(done) {
          chai.request('http://localhost:3000')
          .get('/moneyTracker/v1/user/'+ id_cliente)
          .end(
              function(err,res) {
                console.log("Request has ended");
                console.log(err);
                //console.log(res);
                res.should.have.status(200);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('name');
                res.body.should.have.property('email');
                console.log('email-->' + res.body.email)
  //              res.body.msg.should.be.eql("signUp succesfull");
                done();
              }
          )
      }
    )

    // Detalle de un Usuario - KO
        it('getDetail - Datos KO',
          function(done) {
              chai.request('http://localhost:3000')
              .get('/moneyTracker/v1/user/6b14074629f4d57ab6d09bca')
              .end(
                  function(err,res) {
                    console.log("Request has ended");
                    console.log(err);
                    //console.log(res);
                    res.should.have.status(404);
                    res.should.be.json;
                    res.body.err.should.be.eql("Usuario no localizado");
                    res.body.code.should.be.eql("dataNotfound");
                    console.log ('retorno -> ' + res.body.code);
                    done();
                  }
              )
          }
        )

  // Logout de un usuario
      it('Logout  - OK',
        function(done) {
            chai.request('http://localhost:3000')
            .post('/moneyTracker/v1/user/' + id_cliente + '/logout')
            .end(
                function(err,res) {
                  console.log("Request has ended");
                  console.log(err);
                  //console.log(res);
                  res.should.have.status(200);
                  res.should.be.json;
                  res.body.msg.should.be.eql("logout correcto");
                  done();
                }
            )
        }
      )

  // Logout de un usuario
      it('Logout  - Usuario no logado',
        function(done) {
            chai.request('http://localhost:3000')
            .post('/moneyTracker/v1/user/' + id_cliente + '/logout')
            .end(
                function(err,res) {
                  console.log("Request has ended");
                  console.log(err);
                  //console.log(res);
                  res.should.have.status(409);
                  res.should.be.json;
                  res.body.err.should.be.eql("Usuario no está logado.");
                  done();
                }
            )
        }
      )

      // login OK
                  it('login - OK',
                    function(done) {
                        chai.request('http://localhost:3000')
                        .post('/moneyTracker/v1/login')
                        .set('content-type', 'application/json')
                        .send(
                               {"email": "mjulia.casaslara@gmail.com",
                                "password": "1234567",
              	                }
                        )
                        .end(
                            function(err,res) {
                              console.log("Request has ended");
                              console.log(err);
      //                        console.log(res);
                              res.should.have.status(200);
                              res.should.be.json;
                              res.body.should.have.property('_id');
                              res.body.should.have.property('auth_token');
                              console.log (res.body._id + '/'+ res.body.auth_token);
                              id_cliente = res.body._id;
                              id_token = res.body.auth_token;
                              console.log ('id_cliente --> ' + id_cliente );
                              console.log ('auth token /'+ id_token);
                              res.body.msg.should.be.eql("login correcto");
                              done();
                            }
                        )
                    }
                  )

//TO DO DeLete usuario KO

// TODO DELETE USUARIO ok

}
);
